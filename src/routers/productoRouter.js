const express = require('express');
const ProductoController = require('../controllers/productoController');

class ProductoRouter {

    constructor() {
        this.router = express.Router();
        this.config();
    }

    config() {
        const objProductoC = new ProductoController();
        this.router.post("/producto", objProductoC.resgitrar);
        this.router.get("/producto", objProductoC.getProducto);
        this.router.put("/producto", objProductoC.setProducto);
        this.router.delete("/producto", objProductoC.delete);
    }

}

module.exports = ProductoRouter;