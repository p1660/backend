const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const carroSchema = new Schema({
    fecha: {
        type: String
    },
    id_persona: {
        type: String
    },
    id_producto: {
        type: String
    },
    cantidad: {
        type: String
    }
}, {
    collection: 'carros'
});

module.exports = mongoose.model('Carro', carroSchema);