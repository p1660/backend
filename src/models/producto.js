const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productoSchema = new Schema({
    nombre: {
        type: String
    },
    descripcion: {
        type: String
    },
    precio: {
        type: String
    },
    activo: {
        type: String
    },
    imagen: {
        type: String
    }
}, {
    collection: "productos"
});

module.exports = mongoose.model('Producto', productoSchema);
