const persona = require('../models/persona');

var users = [
    { id: 1, nombre: "Nelson", apellido: "Herrera" },
    { id: 2, nombre: "Ruben", apellido: "Conde" },
    { id: 3, nombre: "Lexly", apellido: "Sosa" },
    { id: 4, nombre: "Andres", apellido: "Quintero" }
];

class UserController {

    constructor() {

    }
    //Obtener todos los usuarios
    getAllUsers(req, res) {
        persona.find((error, data) =>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        })
    }
    /*
    getAllUsers(req, res) {
        res.status(200).json(users);
    }
    */

    getUserById(req, res) {
        let id = req.params.id;
        persona.findById(id, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }
    /*
    //Obtener usuario por id
    getUserById(req, res) {
        let id = req.params.id;
        let tempoUser = null;
        users.forEach(element => {
            if (id == element.id) {
                tempoUser = element;
            }
        });

        if (tempoUser != null) {
            res.status(200).json(tempoUser);
        } else {
            res.status(400).json({ message: 'User not found' });
        }
    }
    */

    register(req, res){
        persona.create(req.body, (error, data)=>{
            if(error){
                res.status(500).json(error);
            }else{
                res.status(201).json(data);
            }
        });
    }
    /*
    //Registrar usuario
    register(req, res) {
        users.push(req.body);
        res.status(201).send();
    }
    */

    deleteUser(req, res){
        let { id } = req.body;
        persona.findByIdAndRemove(id, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }

    update(req, res){
        let { id, nombre, apellido, email, telefono, edad } = req.body;
        let objPersona = {
            nombre,
            apellido,
            email,
            telefono,
            edad
        };
        persona.findByIdAndUpdate(id, {
            $set: objPersona
        }, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        });
    }

    /*
    deleteUser(req, res){
        let { id } = req.body;
        let tempoUsers = [];
        users.forEach(element => {
            if(id != element.id){
                tempoUsers.push(element);
            }
        });

        users = tempoUsers;

        res.status(200).send();
    }
    */

}

exports.default = UserController;