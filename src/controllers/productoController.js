const producto = require('../models/producto');

class ProductoController {

    constructor() {

    }

    //registrar POST
    resgitrar(req, res) {
        producto.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    //consultar GET
    getProducto(req, res) {
        producto.find((error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //actualizar
    setProducto(req, res) {
        let { id, nombre, descripcion, precio, activo, imagen } = req.body;
        let obj = {
            nombre, descripcion, precio, activo, imagen
        };
        producto.findByIdAndUpdate(id, { $set: obj }, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }

    //eliminar
    delete(req, res) {
        let { id } = req.body;
        producto.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        });
    }




}

module.exports = ProductoController;